package com.etf.os2.project.scheduler;

import java.util.LinkedList;
import java.util.List;

import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;

public class CFS extends Scheduler {
	private List<Pcb> list;
	
	public CFS() {
		list = new LinkedList<Pcb>();
	}

	@Override
	public Pcb get(int cpuId) {
		if (list.isEmpty()) return null;
		//return list.remove(0); //bez modifikacije je ovo dovoljno
		
		Pcb p = list.get(0);
		
		for (int i = 0; i < list.size() && list.get(i).getPcbData().lastCPU != -1; i++) 
			if (cpuId == list.get(i).getPcbData().lastCPU) {
				p = list.get(i);
				break;
			}
			
		if (p.getPcbData().executionTimeSum > (1.2*list.get(0).getPcbData().executionTimeSum))
			p = list.get(0);
		
		p.getPcbData().lastCPU = cpuId;
		list.remove(p);
		
		long waitingTime = Pcb.getCurrentTime() - p.getPcbData().startedWaitingTime;
		long timeSlice = waitingTime/Pcb.getProcessCount();
		if (timeSlice==0) timeSlice = 1;
		p.setTimeslice(timeSlice);
		
		return p;
	}

	@Override
	public void put(Pcb pcb) {
		if (pcb == null || pcb == Pcb.IDLE) return;
		PcbData d = pcb.getPcbData();
		if ( d==null ) pcb.setPcbData(d = new PcbData()); 
		d.executionTimeSum += pcb.getExecutionTime();
		d.startedWaitingTime = Pcb.getCurrentTime();
		
		int index = 0;
		while (index < list.size() && (list.get(index)).getPcbData().executionTimeSum <= d.executionTimeSum)	
			index++;
		
		list.add(index,pcb);
	}

}
