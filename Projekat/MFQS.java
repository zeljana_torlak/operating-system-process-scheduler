package com.etf.os2.project.scheduler;

import java.util.LinkedList;
import java.util.List;

import com.etf.os2.project.process.Pcb;
import com.etf.os2.project.process.PcbData;

public class MFQS extends Scheduler {
	private long[] TimeSlice;
	private List<Pcb> list[];
	
	public MFQS(int num, long[] TimeSlice) {
		list = new LinkedList[num];
		for (int i = 0; i < num; i++)
			list[i] = new LinkedList<Pcb>();
		this.TimeSlice = TimeSlice;
	}

	@Override
	public Pcb get(int cpuId) {
		/*for (int i = list.length-1; i >= 0; i--) {
			if ( !list[i].isEmpty() ) return list[i].remove(0);
		} 
		return null;*/ //bez modifikacije je ovo dovoljno sa obrnutim prioritetima
		
		Pcb p = null;
		int index = -1;
		
		for (int i = 0; i < list.length; i++) {
			if ( !list[i].isEmpty()) {
				p = list[i].get(0);
				index = i;
				break;
			}
		}	
			
		if ( index==-1 ) return null;
		
		for (int j = 0; j < list[index].size() && list[index].get(j).getPcbData().lastCPU != -1; j++) 
			if (cpuId == list[index].get(j).getPcbData().lastCPU) {
				p = list[index].get(j);
				break;
			}
		
		list[index].remove(p);
		p.getPcbData().lastCPU = cpuId;
		return p;
	}

	@Override
	public void put(Pcb pcb) {
		if (pcb == null || pcb == Pcb.IDLE) return;
		PcbData d = pcb.getPcbData();
		if ( d==null ) pcb.setPcbData(d = new PcbData()); 
		
		int prio = 0;
		if (pcb.getPreviousState() == Pcb.ProcessState.CREATED)
			prio = pcb.getPriority();
		else if (pcb.getPreviousState() == Pcb.ProcessState.BLOCKED) 
			prio = d.newPriority - 1;
		else //isteklo cpu vreme
			prio = d.newPriority + 1;
		
		if ( prio < 0 ) prio = 0;
		if ( prio >= list.length ) prio = list.length-1;
		d.newPriority = prio;
		
		pcb.setTimeslice(TimeSlice[prio]);
		list[prio].add(pcb);
		
	}

}
