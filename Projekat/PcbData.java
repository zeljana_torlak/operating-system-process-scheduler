package com.etf.os2.project.process;

public class PcbData {
	public double prediction;
	public int newPriority;
	public long executionTimeSum;
	public long startedWaitingTime;
	
	public int lastCPU = -1;
}
