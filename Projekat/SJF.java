package com.etf.os2.project.scheduler;

import java.util.*;

import com.etf.os2.project.process.*;

public class SJF extends Scheduler {
	private double coeff;
	private boolean isPreemptive;
	
	private List<Pcb> list;

	public SJF(double coeff, boolean isPreemptive) {
		if ( coeff < 0 || coeff > 1 ) return;
		this.coeff = coeff;
		this.isPreemptive = isPreemptive;
		list = new LinkedList<Pcb>();
	}

	//tau(n+1) = coeff*t(n) + (1-coeff)*tau(n), 0 <= coeff <= 1
	
	@Override
	public Pcb get(int cpuId) {
		if (list.isEmpty()) return null;
		//return list.remove(0); //bez modifikacije je ovo dovoljno
		
		Pcb p = list.get(0);
		for (int i = 0; i < list.size() && list.get(i).getPcbData().lastCPU != -1; i++) 
			if (cpuId == list.get(i).getPcbData().lastCPU) {
				p = list.get(i);
				break;
			}
		
		if (p.getPcbData().prediction > (1.2*list.get(0).getPcbData().prediction))
			p = list.get(0);
		
		p.getPcbData().lastCPU = cpuId;
		list.remove(p);
		return p;
	}
	
	@Override
	public void put(Pcb pcb) {
		if (pcb == null || pcb == Pcb.IDLE) return;
		PcbData d = pcb.getPcbData();
		if ( d==null ) pcb.setPcbData(d = new PcbData()); 
		
		if ( pcb.getPreviousState() != Pcb.ProcessState.READY )
			d.prediction = coeff*pcb.getExecutionTime() + (1-coeff)*d.prediction;
		
		//kad dodje do preuzimanja da li azururirati prediction?
		
		if ( isPreemptive ) {
			
			double min = -1;
			int minInd = 0;
			for (int i = 0; i<Pcb.RUNNING.length; i++)
				if (Pcb.RUNNING[i]!=null && Pcb.RUNNING[i].getPcbData()!=null &&
				Pcb.RUNNING[i].getPcbData().prediction > d.prediction && Pcb.RUNNING[i].getPcbData().prediction > min) {
					min = Pcb.RUNNING[i].getPcbData().prediction;
					minInd=i;
				}
			if (min!=-1) {
				Pcb.RUNNING[minInd].preempt();
			}
		}
		
		int index = 0;
		while (index < list.size() && list.get(index).getPcbData().prediction <= d.prediction)	
			index++;
		
		pcb.setTimeslice(0);
		list.add(index,pcb);
	}
	
}
