package com.etf.os2.project.scheduler;

import com.etf.os2.project.process.Pcb;

public abstract class Scheduler {
    public abstract Pcb get(int cpuId);

    public abstract void put(Pcb pcb);

    public static Scheduler createScheduler(String[] args) {
    	
    	String name = args[0];
		
		if ( name.equals("SJF") && args.length >= 3 ) {
			double coeff = Double.parseDouble(args[1]);
			boolean isPreemptive = ( (Integer.parseInt(args[2]))!=0 ? true : false);
			return new SJF(coeff,isPreemptive);
		}
		
		if ( name.equals("MFQS") && args.length >= 2 ) {
			int num = Integer.parseInt(args[1]);
			long[] TimeSlice = new long[num]; 
			for (int i = 0; i<num; i++) {
				if (args[i+2].isEmpty()) return null;
				TimeSlice[i] = Long.parseLong(args[i+2]);
			}
			return new MFQS(num,TimeSlice);
		}
		
		if ( name.equals("CFS") ) 
			return new CFS();
		
		return null;
    }
}
